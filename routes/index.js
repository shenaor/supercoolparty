
/*
 * GET home page.
 */
var app = require('../app');

var  storeName="Super Cool Party"
	,	 products = require('./products');


function Product(id,name,category,image,description,price,keyword){
	this.id = id;
	this.name = name;
	this.category = category;
	this.description = description;
	this.price = price;
	this.image = image;
	//specify keyword as an array in mongoose scheme
	this.keyword = keyword;
}

var products =[
	new Product(1, '10 inch Green', 'GlowProducts', '/images/glow/10-inch-green-sg.jpg', 'Glows in the Dark', 100),
	new Product(2, '22 inch Assorted', 'GlowProducts', '/images/glow/22-inch-assorted-premium.jpg'),
	new Product(3, '22 inch Blue', 'GlowProducts', '/images/glow/22-inch-blue.jpg'),
	new Product(4, '22 inch Gyo', 'GlowProducts', '/images/glow/22-inch-gyo.jpg'),
	new Product(5, 'Afro Wig', 'CrazyHour', '/images/crazyh/afrowig.jpg'),
	new Product(6, 'Bad Wolf', 'CrazyHour', '/images/crazyh/badwoolf.jpg'),
	new Product(7, 'Beer Hat', 'CrazyHour', '/images/crazyh/beerhat.jpg'),
	new Product(8, 'Boa', 'CrazyHour', '/images/crazyh/boa.jpg'),
	new Product(9, '10 inch Blue', 'GlowProducts', '/images/glow/10-inch-blue-sg.jpg'),
	new Product(10, '22 inch White', 'GlowProducts', '/images/glow/22-inch-white.jpg'),
	new Product(11, '4 inch Assorted', 'GlowProducts', '/images/glow/4-inch-assorted.jpg'),
	new Product(12, '4 inch Pink',  'GlowProducts', '/images/glow/4-inch-pink.jpg'),
	new Product(12, '4 inch White', 'GlowProducts', '/images/glow/4-inch-white.jpg'),
	new Product(13, '6 inch Safety HI Green', 'GlowProducts', '/images/glow/6-inch-safety-HI-green.jpg'),
	new Product(14, '6 inch Safety Yellow', 'GlowProducts', '/images/glow/6-inch-safety-yellow.jpg'),
	new Product(15, '6 inch White', 'GlowProducts', '/images/glow/6-inch-white.jpg'),
	new Product(16, '8 inch Bracelet', 'GlowProducts', '/images/glow/8-inch-bracelete.jpg'),
	new Product(17, 'Glow Glasses', 'GlowProducts', '/images/glow/glow-glasses-sg.jpg'),
	new Product(18, 'Glow Leis', 'GlowProducts', '/images/glow/glow-leis-sg.jpg'),
	new Product(19, 'Glow Disc Frisbee LED', 'GlowProducts','/images/glow/GLOW_DISC_FRISBEE_LED.jpg')
];


exports.index = function(req, res){
  res.render('index',{
  	storeName:storeName,
  	title :storeName
  });
};

exports.partySolutions = function(req,res){
	res.render('./Categories/partySolutions.jade',{
		products:products,
		title:storeName + '- Party Solutions',
		storeName:storeName
	});
};

exports.glowP = function(req,res){
	res.render('./Categories/GlowProducts',{
			products:products,
			title: storeName + ' - Glow Products',
			storeName:storeName
		});
};

exports.partySupplies = function(req,res){
	res.render('./Categories/PartySupplies',{
		products:products,
		title: storeName + ' - Party Supplies',
		storeName:storeName
	});
};

exports.led = function(req,res){
	res.render('./Categories/LED',{
		products:products,
		title: storeName + ' - LED / Flash Products',
		storeName:storeName
	});
};

exports.crazyH = function(req,res){
	res.render('./Categories/CrazyHour',{
		products:products,
		title: storeName + ' - Crazy Hour Products',
		storeName:storeName
	});
};

exports.productPage = function(req,res){
		var name = products[req.params.id].name
    ,		id = product[req.params.id].id    
    ,   price = product[req.params.id].price;
	 res.render('product',{ title: storeName+ product.name, id:id,name:name, price:price });
}