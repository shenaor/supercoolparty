
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose')
  , db = mongoose.createConnection('localhost','test')
  , products = require('./routes/products');


var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

//Configurations
app.configure('development', function(){
  app.use(express.errorHandler());
  app.locals.pretty =true;
});

app.configure('deployment',function(){
	app.set('port',process.env.VMC_APP_PORT || 1337, null);
});

app.locals.errors={};
app.locals.message={};


// Routes

app.get('/', routes.index);
app.get('/Categories/partySolutions',routes.partySolutions);
app.get('/Categories/GlowProducts', routes.glowP);
app.get('/Categories/PartySupplies', routes.partySupplies);
app.get('/Categories/LED', routes.led);
app.get('/Categories/CrazyHour', routes.crazyH);
app.get('/product:id',routes.productPage);

//mongoose connection

db.on('error',console.error.bind(console,'connection error:'));
db.once('open',function(){
  console.log("good");
});


//Service init
http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
